# snapnet-master

#### 介绍
点云-图分割方案

#### 软件架构
基于ubuntu16.04,cuda9.0,tensorflow1.9,cudnn7.1,gtx1080,pcl,vtk


#### 安装教程

1. 安装ubuntu,cuda9,cudnn7.1,安装libpcl,libvtk,openmp
2. 安装pycharm,anaconda
3. 安装tensorflow,cython,numpy,scipy,tqdm

#### 使用说明
snapnet脚本
cd pointcloud_tools
python setup.py install --home="."
主要是生成点云处理工具,成功之后的目录结构

build
├── lib.linux-x86_64-3.6
│   └── PcTools.cpython-36m-x86_64-linux-gnu.so
└── temp.linux-x86_64-3.6
    ├── pointCloudLabels.o
    ├── pointCloud.o
    ├── Sem3D.o
    └── Semantic3D.o

lib
└── python
    ├── PcTools.cpython-36m-x86_64-linux-gnu.so
    └── PointCloud_tools-0.0.0-py3.6.egg-info

# 初始化快照
python sem3d_gen_images.py --config config.json
相机数量应当修改至500-800，会慢很多但结果会更好

# 训练模型
python sem3d_train_tf.py --config config.json

# 反向映射至三维空间
python sem3d_test_backproj_tf.py --config config.json

# 最后以Semantic 3D格式生成文件并为原始点云的每个点分配标签
python sem3d_test_to_sem3D_labels.py --config config.json


#### 参与贡献
"Unstructured point cloud semantic labeling using deep segmentation networks", 
A.Boulch and B. Le Saux and N. Audebert, Eurographics Workshop on 3D Object Retrieval 2017
https://github.com/aboulch/snapnet#snapnet-point-cloud-semantization-using-deep-segmentation-network


#### 码云特技
欢迎交流792372226@qq.com






![分类截图1](https://images.gitee.com/uploads/images/2019/0527/145053_242b2845_1974806.png "00101.png")
![分类截图2](https://images.gitee.com/uploads/images/2019/0527/145126_7c71af4d_1974806.png "01515.png")